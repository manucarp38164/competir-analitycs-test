"""server URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from vicente_lopez import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url('develop/v1/login', views.Login.as_view(), name='tokens'),
    url('develop/v1/user/add', views.NewUserAdd.as_view(), name='user'), ##Post user created
    url('develop/v1/vlopez/content/general', views.Content.as_view(), name='contentGeneral1'), ##POST and GET Content Consumption
    url('production/v1/vlopez/content/general', views.Content.as_view(), name='contentGeneral1'),  ##POST and GET Content Consumption
    url('develop/v1/vlopez/tablet/active', views.TabletHistory.as_view(), name='contentGeneral2'), ##M1
    url('production/v1/vlopez/tablet/active', views.TabletHistory.as_view(), name='contentGeneral2'),
    url('develop/v1/vlopez/content/today', views.ContentConsumed.as_view(), name='contentGeneral3'),
    url('production/v1/vlopez/content/today', views.ContentConsumed.as_view(), name='contentGeneral3'),
    url('develop/v1/vlopez/launcher/today', views.LauncherUse.as_view(), name='contentGeneral4'),
    url('production/v1/vlopez/launcher/today', views.LauncherUse.as_view(), name='contentGeneral4'),
    url('develop/v1/vlopez/app/use', views.UseApp.as_view(), name='contentGeneral9'),
    url('develop/v1/vlopez/content/download', views.ContentDownload.as_view(), name='contentGeneral10'), ##POST
    url('develop/v1/vlopez/words/search', views.SearchesWords.as_view(), name='contentGeneral11'), ##POST
    url('develop/v1/logout', views.Logout.as_view(), name='user'), ##GET
    url('develop/v1/vlopez/app/download', views.DownloadApp.as_view(), name='App'), ##POST
    url('develop/v1/vlopez/user/disable', views.DisabledEnabledUser.as_view(), name='user'), ##POST
    url('develop/v1/vlopez/projects', views.AvailableProjects.as_view(), name='AvailableProjects') ##GET


]
