import datetime

import pandas as pd
import numpy as np
import json

from cassandra.util import Date, Time

from ..classes.cql_singleton import Singleton as cql
from ..classes.dataBaseSingleton import DataBaseSingleton
import threading



content = pd.DataFrame()
session = DataBaseSingleton.getSession()


def calculate_content_consumption(category, date_from, date_to, school_filter):
    data = cql.getContentTable()
    if school_filter is not None:
        data = data.loc[data['school_code'] == school_filter, :]

    if category:
        if str(category) != "All":
            data = data[data["category"] == str(category)]
        if (date_from is not None) & (date_from != ""):
            if (date_to is not None) & (date_to != ""):
                if Date(date_to) < Date(date_from):
                    return json.dumps({})
                else:
                    data = data[(data["date"] >= Date(date_from)) & (data["date"] <= Date(date_to))]
            else:
                data = data[(data["date"] >= Date(date_from)) & (data["date"] <= Date(datetime.date.today()))]

    response = pd.DataFrame(data['content_type'].value_counts())
    return response.to_json()


def get_content_consumption_by_category(df):
    data = pd.DataFrame(list(df))
    categories = data.category.unique()
    content_types = data.content_type.unique()
    data_json = [{}]
    first = True
    for r_type in content_types:
        resources = {}
        first_content_type = True

        for category in categories:
            df = data[data["category"] == str(category)]
            res = df.loc[(df['category'] == category) & (df['content_type'] == r_type), ['category', 'content_type']]
            test = {
                category: int(res['content_type'].count())
            }
            if first_content_type:
                resources = test
                first_content_type = False
            else:
                for d in (resources, test): resources.update(d)
        da = [{
            r_type: resources
        }]
        if first:
            data_json = da
            first = False
        else:
            data_json = data_json + da
    return json.dumps(data_json)
