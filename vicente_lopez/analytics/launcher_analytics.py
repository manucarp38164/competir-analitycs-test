import datetime

import pandas as pd
import numpy as np
import json

from cassandra.util import Date, Time

from ..classes.cql_singleton import Singleton as cql
from ..classes.dataBaseSingleton import DataBaseSingleton
import threading
from ..models import *


content = pd.DataFrame()
session = DataBaseSingleton.getSession()


def content_consumed_today():
    data = cql.getContentTodayTable()
    if data.empty:
        response = {}
    else:
        quantity = int(data['content_name'].value_counts().count())
        response = {
            "content_consumed": quantity
        }
    print(response)
    return json.dumps(response)


def tablet_today():
    data = cql.getContentTodayTable()
    if data.empty:
        response = {}
    else:
        quantity = int(data['serial_tablet'].value_counts().count())
        response = {
            "activ_tablet": quantity
        }
        insert = TabletHistory.create(date= datetime.date.today(),
                                      tablet_amount=quantity)
        insert.save()
    return json.dumps(response)


def launcher_use():
    data = cql.getLauncherUseToday()
    if data.empty:
        response = {}
    else:
        tablet = data['serial_tablet'].nunique()
        total_time = data['seconds_elapsed'].sum() / 60
        response = {
            "average_time": total_time / tablet
        }
        insert = TabletHistory.create(date=datetime.date.today(),
                                      average_time=total_time / tablet)
        insert.save()
    return json.dumps(response)


def get_content_consumption_by_category(df):
    data = pd.DataFrame(list(df))
    categories = data.category.unique()
    content_types = data.content_type.unique()
    data_json = [{}]
    first = True
    for r_type in content_types:
        resources = {}
        first_content_type = True

        for category in categories:
            df = data[data["category"] == str(category)]
            res = df.loc[(df['category'] == category) & (df['content_type'] == r_type), ['category', 'content_type']]
            test = {
                category: int(res['content_type'].count())
            }
            if first_content_type:
                resources = test
                first_content_type = False
            else:
                for d in (resources, test): resources.update(d)
        da = [{
            r_type: resources
        }]
        if first:
            data_json = da
            first = False
        else:
            data_json = data_json + da
    return json.dumps(data_json)
