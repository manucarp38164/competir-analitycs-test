from django.apps import AppConfig


class VicenteLopezConfig(AppConfig):
    name = 'vicente_lopez'
