import json

class AppUse():
    def __init__(self,app_name, date, end_time, init_time, latitude, longitude, serial_tablet, time, category):
        self.app_name=app_name
        self.date=date
        self.end_time=end_time
        self.init_time=init_time
        self.latitude=latitude
        self.longitude=longitude
        self.serial_tablet=serial_tablet
        self.time=time
        self.category=category

    @classmethod
    def from_json(cls, json_string):
        json_dict = json.loads(json_string)
        return cls(**json_dict)


class AppDownload():
    def __init__(self,app_name, category, date, external_app, latitude, longitude, serial_tablet, time, uninstalled ):
        self.app_name=app_name
        self.category = category
        self.date=date
        self.external_app = external_app
        self.latitude=latitude
        self.longitude=longitude
        self.serial_tablet=serial_tablet
        self.time=time
        self.uninstalled=uninstalled


    @classmethod
    def from_json(cls, json_string):
        json_dict = json.loads(json_string)
        return cls(**json_dict)
