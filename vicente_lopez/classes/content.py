import json


class Consumption():
    def __init__(self, category, content_type, content_name, date, time, latitude, longitude, serial_tablet ):
        self.category = category
        self.content_type = content_type
        self.content_name = content_name
        self.date = date
        self.time = time
        self.latitude = latitude
        self.longitude = longitude
        self.serial_tablet  = serial_tablet

    @classmethod
    def from_json(cls, json_string):
        json_dict = json.loads(json_string)
        return cls(**json_dict)


class DownloadContent():
    def __init__(self, category, content_name, date, time, latitude,  longitude, school_code, serial_tablet ):
        self.category = category
        self.content_name = content_name
        self.date = date
        self.time = time
        self.latitude = latitude
        self.longitude = longitude
        self.school_code = school_code
        self.serial_tablet  = serial_tablet

    @classmethod
    def from_json(cls, json_string):
        json_dict = json.loads(json_string)
        return cls(**json_dict)