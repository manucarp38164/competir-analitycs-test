import datetime

from .dataBaseSingleton import DataBaseSingleton
import threading
import pandas as pd

session = DataBaseSingleton.getSession()
class Singleton():
    __instance = None
    __content = pd.DataFrame()
    __school = pd.DataFrame()
    __contentToday = pd.DataFrame()
    __launcherUse = pd.DataFrame()
    @staticmethod
    def getInstance():
        """ Static access method. """
        if Singleton.__instance == None:
            Singleton()
        return Singleton.__instance

    @staticmethod
    def getContentTable():
        """ Static access method. """
        if Singleton.__content.empty:
            Singleton()
        return Singleton.__content

    @staticmethod
    def getContentTodayTable():
        """ Static access method. """
        if Singleton.__contentToday.empty:
            Singleton()
        return Singleton.__contentToday

    @staticmethod
    def getLauncherUseToday():
        """ Static access method. """
        if Singleton.__launcherUse.empty:
            Singleton()
        return Singleton.__launcherUse

    @staticmethod
    def getSchoolTable():
        """ Static access method. """
        if Singleton.__school.empty:
            Singleton()
        return Singleton.__school

    def setContent(self, content):
        self.__content = content

    def __init__(self):
        """ Virtually private constructor. """
        if Singleton.__school.empty:
            Singleton.__school = pd.DataFrame(session.execute("SELECT * FROM school"))
        if Singleton.__content.empty:
            Singleton.__content = pd.DataFrame(session.execute("SELECT * FROM content_consumption;"))
        else:
            now = datetime.date.today()
            today = pd.DataFrame(session.execute("SELECT * FROM content_consumption WHERE date='" + str(now) + "' ALLOW FILTERING;"))
            print(today.head())
            if not today.empty:
                Singleton.__content = pd.merge(Singleton.__content, today, how='outer')

        nowD = datetime.date.today()
        Singleton.__contentToday = pd.DataFrame(session.execute("SELECT * FROM content_consumption WHERE date='" + str(nowD) + "' ALLOW FILTERING;"))
        Singleton.__launcherUse = pd.DataFrame(session.execute("SELECT * FROM launcher WHERE date='" + str(nowD) + "' ALLOW FILTERING;"))



    @staticmethod
    def setConsult():
        Singleton()
        timer = threading.Timer(30, Singleton.setConsult)
        timer.start()


# timer = threading.Timer(600, Singleton.setConsult)
# timer.start()

