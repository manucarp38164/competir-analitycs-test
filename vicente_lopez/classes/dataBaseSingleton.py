from cassandra.cluster import Cluster


class DataBaseSingleton:
    __instance = None
    cluster = Cluster(['190.52.34.41'], port=9042)
    __session = cluster.connect()
    __session.set_keyspace('vlos_db')

    @staticmethod
    def getInstance():
        """ Static access method. """
        if DataBaseSingleton.__instance == None:
            DataBaseSingleton()
        return DataBaseSingleton.__instance

    @staticmethod
    def getSession():
        """ Static access method. """
        if DataBaseSingleton.__session == None:
            DataBaseSingleton()
        return DataBaseSingleton.__session

    def __init__(self):
        """ Virtually private constructor. """
        if DataBaseSingleton.__instance != None:
            raise Exception("This class is a singleton!")
        else:
            DataBaseSingleton.__instance = self