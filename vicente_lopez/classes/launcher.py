import json


class Use():
    def __init__(self, seconds_elapsed, date, time, latitude, longitude, serial_tablet):
        self.seconds_elapsed = seconds_elapsed
        self.date = date
        self.time = time
        self.latitude = latitude
        self.longitude = longitude
        self.serial_tablet = serial_tablet

    @classmethod
    def from_json(cls, json_string):
        json_dict = json.loads(json_string)
        return cls(**json_dict)
