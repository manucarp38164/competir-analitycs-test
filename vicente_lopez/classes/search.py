import json

class Word():
    def __init__(self, word_searched, result, date, time, latitude, longitude, serial_tablet):
        self.word_searched = word_searched
        self.result = result
        self.date = date
        self.time = time
        self.latitude = latitude
        self.longitude = longitude
        self.serial_tablet = serial_tablet

    @classmethod
    def from_json(cls, json_string):
        json_dict = json.loads(json_string)
        return cls(**json_dict)