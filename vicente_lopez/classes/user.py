import json

class UserAuth():
    def __init__(self, username, password):
        self.username = username
        self.password = password

    @classmethod
    def from_json(cls, json_string):
        json_dict = json.loads(json_string)
        return cls(**json_dict)


class NewUser():
    def __init__(self, password, is_superuser, username, first_name, last_name, email,is_staff, user_organization, user_rol, metrics):
        self.password = password
        self.is_superuser = is_superuser
        self.username = username
        self.first_name = first_name
        self.last_name =last_name
        self.email = email
        self.is_staff = is_staff
        self.user_organization =user_organization
        self.user_rol = user_rol
        self.metrics = metrics


    @classmethod
    def from_json(cls, json_string):
        json_dict = json.loads(json_string)
        return cls(**json_dict)


class MetricUser():
    def __init__(self, metric_id, user_id):
        self.metric_id = metric_id
        self.user_id = user_id


    @classmethod
    def from_json(cls, json_string):
        json_dict = json.loads(json_string)
        return cls(**json_dict)

class User():
    def __init__(self, username):
        self.username = username

    @classmethod
    def from_json(cls, json_string):
        json_dict = json.loads(json_string)
        return cls(**json_dict)

