import datetime
from math import radians, sin, cos, acos
import pandas as pd
import json
from calendar import monthrange
from ..classes import cql_singleton


def calculate_distance(first_lat, first_long, second_lat, second_long):
    return (6371.01 * acos(sin(radians(first_lat))*sin(radians(second_lat)) + cos(radians(first_lat))*cos(radians(second_lat))*cos(radians(first_long) - radians(second_long)))) * 1000


def get_schools(df):
    data = pd.DataFrame(list(df))
    school_data = []
    for index, row in data.iterrows():
        test = {
            "school_name": row['school_name'],
            "school_code": row['school_code'],
        }
        school_data.append(test)

    return json.dumps(school_data)


def get_current_year_and_last_day_of_month(month):
    return int(datetime.datetime.now().year), monthrange(int(datetime.datetime.now().year), int(month))[1]


def get_school_code(lat, long):
    school_data = cql_singleton.Singleton.getSchoolTable()
    school_code = "N/R"
    for index, row in school_data.iterrows():
        if calculate_distance(lat, long, row[1], row[2]) < row[3]:
            return str(row[4])
    return school_code