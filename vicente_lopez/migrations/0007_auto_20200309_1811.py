# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2020-03-09 21:11
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('vicente_lopez', '0006_metrics_project'),
    ]

    operations = [
        migrations.RenameField(
            model_name='metrics',
            old_name='id',
            new_name='id_metrics',
        ),
    ]
