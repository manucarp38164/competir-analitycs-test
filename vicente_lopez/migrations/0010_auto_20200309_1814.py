# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2020-03-09 21:14
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('vicente_lopez', '0009_metrics'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Metrics',
            new_name='MetricsTable',
        ),
    ]
