# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2020-03-09 21:15
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('vicente_lopez', '0010_auto_20200309_1814'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='metricstable',
            name='project',
        ),
    ]
