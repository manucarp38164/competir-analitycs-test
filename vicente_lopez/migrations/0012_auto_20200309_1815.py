# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2020-03-09 21:15
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('vicente_lopez', '0011_remove_metricstable_project'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='metricstable',
            name='user',
        ),
        migrations.RemoveField(
            model_name='project',
            name='user',
        ),
        migrations.DeleteModel(
            name='MetricsTable',
        ),
        migrations.DeleteModel(
            name='Project',
        ),
    ]
