from django.db import models

from django.contrib.auth.models import User
import uuid
from cassandra.cqlengine import columns
from cassandra.cqlengine.models import Model
import json


# Model for Resouces Consumtion, We can see the resource type most used - Metricas 1 y 2
class ContentConsumption(Model):
    id = columns.UUID(primary_key=True, default=uuid.uuid4)
    category = columns.Text(required=True)
    content_type = columns.Text(required=True)
    content_name = columns.Text(required=True)
    school_code = columns.Text(required=True)
    date = columns.Date(required=True)
    time = columns.Time(required=True)
    latitude = columns.Float(required=True)
    longitude = columns.Float(required=True)
    serial_tablet  = columns.Text(required=True)


# Model for Word Searches, We can see the most searched words by tablet, school or in general - Metrica 3
class WordSearches(Model):
    id = columns.UUID(primary_key=True, default=uuid.uuid4)
    word_searched = columns.Text(required=True)
    result = columns.Integer(required=True)
    date = columns.Date(required=True)
    time = columns.Time(required=True)
    latitude = columns.Float(required=True)
    longitude = columns.Float(required=True)
    serial_tablet  = columns.Text(required=True)


# Model for App Status, We can see if an app was enabled(Play Store Apps) or downloaded(Server Apps) - Metrica 4
class AppStatus(Model):
    id = columns.UUID(primary_key=True, default=uuid.uuid4)
    app_name = columns.Text(required=True)
    category = columns.Text(required=True)
    external_app = columns.Boolean(default=0)
    uninstalled = columns.Boolean(default=0)
    date = columns.Date(required=True)
    time = columns.Time(required=True)
    latitude = columns.Float(required=True)
    longitude = columns.Float(required=True)
    serial_tablet  = columns.Text(required=True)


# Model for App Activity, We can see the time that an app were used and if it was uninstalled - Metrica 4
class AppActivity(Model):
    id = columns.UUID(primary_key=True, default=uuid.uuid4)
    app_name = columns.Text(required=True)
    init_time = columns.Time(default=None)
    end_time = columns.Time(default=None)
    date = columns.Date(required=True)
    time = columns.Time(required=True)
    latitude = columns.Float(required=True)
    longitude = columns.Float(required=True)
    serial_tablet = columns.Text(required=True)
    category = columns.Text(required=True)


# Model for Resouce Downloads, We can see the most downloaded specific resource(name) - Metrica 5
class ContentDownloads(Model):
    id = columns.UUID(primary_key=True, default=uuid.uuid4)
    category = columns.Text(required=True)
    content_name = columns.Text(required=True)
    school_code = columns.Text(required=True)
    date = columns.Date(required=True)
    time = columns.Time(required=True)
    latitude = columns.Float(required=True)
    longitude = columns.Float(required=True)
    serial_tablet  = columns.Text(required=True)


# Model for Tablet Status, We can see the battery status(damage, OK, etc) and the battery percentage
class TabletStatus(Model):
    id = columns.UUID(primary_key=True, default=uuid.uuid4)
    date = columns.Date(required=True)
    time = columns.Time(required=True)
    latitude = columns.Float(required=True)
    longitude = columns.Float(required=True)
    serial_tablet  = columns.Text(required=True)
    battery_percentage = columns.Float(required=True)
    battery_status = columns.Text(required=True)
    tablet_manufacturer = columns.Text(required=True)
    os_version = columns.Text(required=True)


class School(Model):
    id = columns.UUID(primary_key=True, default=uuid.uuid4)
    school_name = columns.Text(required=True)
    latitude = columns.Float(required=True)
    longitude = columns.Float(required=True)
    ratio = columns.Integer(required=True)
    school_code = columns.Text(required=True)


class Launcher(Model):
    id = columns.UUID(primary_key=True, default=uuid.uuid4)
    seconds_elapsed = columns.Integer(required=True)
    latitude = columns.Float(required=True)
    longitude = columns.Float(required=True)
    serial_tablet = columns.Text(required=True)
    date = columns.Date(required=True)
    time = columns.Time(required=True)


class TabletHistory(Model):
    date = columns.Date(required=True, primary_key=True)
    average_time = columns.Integer(required=False)
    tablet_amount = columns.Integer(required=False)


# Model tables Mysql

class Project(models.Model):
    id = models.AutoField (primary_key = True)
    name = models.CharField(max_length=30, blank=False)
    image = models.CharField(max_length=200, blank=True)





class SubItem(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100, blank=False)
    father_id = models.IntegerField()
    active = models.BooleanField()
    category = models.BooleanField()



class Metric(models.Model):
    id_metrics = models.AutoField (primary_key = True)
    name = models.CharField(max_length=100, blank=False)
    project = models.ForeignKey(Project, on_delete=models.CASCADE, null=False, blank=False)
    subItem = models.ForeignKey(SubItem, on_delete=models.CASCADE, null=False, blank=False)


class UserCustom(User):
    user_organization = models.CharField(max_length=30, blank=True)
    user_rol = models.CharField(max_length=30, blank=True)
    metrics = models.ManyToManyField(Metric, through='UserMetrics')


class UserMetrics(models.Model):
    user = models.ForeignKey(UserCustom, on_delete=models.CASCADE, null=False, blank=False)
    metric = models.ForeignKey(Metric, on_delete=models.CASCADE, null=False, blank=False)





















