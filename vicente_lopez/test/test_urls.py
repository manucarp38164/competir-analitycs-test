from django.urls import reverse, resolve
from vicente_lopez.views import Login, AvailableProjects, Content, TabletHistory, ContentConsumed, LauncherUse


class TestUrls:

    def test_available_projects_resolves(self):
        url = reverse('AvailableProjects')
        assert(resolve(url).func.view_class, AvailableProjects)

    def test_content_general1_resolves(self):
        url = reverse('contentGeneral2')
        assert(resolve(url).func.view_class, Content)

    def test_content_general2_resolves(self):
        url = reverse('contentGeneral2')
        assert(resolve(url).func.view_class, TabletHistory)

    def test_content_general3_resolves(self):
        url = reverse('contentGeneral3')
        assert(resolve(url).func.view_class, ContentConsumed)

    def test_content_general4_resolves(self):
        url = reverse('contentGeneral4')
        assert(resolve(url).func.view_class, LauncherUse)
