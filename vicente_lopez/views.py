import datetime
import pandas as pd

from django.http import HttpResponse
from cassandra.cluster import Cluster
from rest_framework.permissions import IsAuthenticated
from django.contrib.auth.models import User

from vicente_lopez.models import *
from .analytics import *
from .classes.content import *
from .classes.launcher import *

from django.contrib.auth.models import User


from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.authtoken.models import Token
import json

from django.contrib.auth import authenticate
from django.contrib.auth import login as do_login
from django.contrib.auth import logout as do_logout
from cassandra.util import Date
from .helpers.helper import *
from .analytics import content_analytics, launcher_analytics
from .classes.application import AppUse, AppDownload
from .classes.user import UserAuth, NewUser, MetricUser
from .classes.search import Word

from django.db import connection


class Login(APIView):
    def post(self, request):
        if bool(request.body):
            user_data = json.loads(request.body)
            user = UserAuth(**user_data)
            usr=authenticate(username=user.username, password=user.password)
            if usr:
                us, created = User.objects.get_or_create(username=user.username)
                if created:
                    us.set_password(user.password)
                    us.save()
                token, created = Token.objects.get_or_create(user=us)
                id_user = token.user_id
                cursor = connection.cursor()
                # obtener proyectos
                sql_p="Select Distinct u.user_ptr_id,p.id, p.name, p.image from vicente_lopez_usercustom as u inner join vicente_lopez_usermetrics as um on um.user_id=u.user_ptr_id inner join vicente_lopez_metric as m on m.id_metrics=um.metric_id inner join vicente_lopez_project as p on p.id=m.project_id where u.user_ptr_id ='"+str(id_user) +"'"
                cursor.execute(sql_p)
                data_p = cursor.fetchall()
                projects =[]
                for p in data_p:
                    project ={
                        "id_project":p[1],
                        "project_name":p[2],
                        "image":p[3]
                        }
                    projects.append(project)

                content = {
                        "user_name":user.username,
                        "token": str(token.key),
                        "project": projects
                    }


                return HttpResponse(json.dumps(content), 'application/json' ,status=200, reason="authenticate")
            else:
                content = {
                    "message": "Login failed, Username or password invalid"
                    }
                return HttpResponse(json.dumps(content), 'application/json' ,status=401)
        else:
            return HttpResponse(status=403)


class Logout(APIView):
    # permission_classes = (IsAuthenticated,)
    def post(self, request):
        auth_header = request.META.get('HTTP_AUTHORIZATION')
        a = Token.objects.get(key=auth_header)

        if a is not None:
            a.delete()
            return HttpResponse(json.dumps({"menssage": "Logout, successful"}), content_type='application/json')


class NewUserAdd(APIView):
    # permission_classes = (IsAuthenticated,)
    def post(self, request):
        if request.method == "POST":
            if bool(request.body):
                new_user = json.loads(request.body)
                user = NewUser(**new_user)
                try:
                    us, insert = UserCustom.objects.get_or_create(is_superuser=user.is_superuser,
                                                                    username=user.username,
                                                                    first_name=user.first_name,
                                                                    last_name=user.last_name,
                                                                    email=user.email,
                                                                  is_staff=user.is_staff,
                                                    user_organization=user.user_organization,
                                                    user_rol=user.user_rol)
                    for m in user.metrics:
                        mu, insert = UserMetrics.objects.get_or_create(user_id=us.id,
                                                                       metric_id=m)
                        mu.save()



                except:
                    content = {
                        "message":"User already exist"
                    }
                    return HttpResponse(json.dumps(content), status=202,reason="Duplicate")

                if insert:
                    us.set_password(user.password)
                    us.save()
                    content = {
                        "message": "User created"
                    }
                    return HttpResponse(json.dumps(content),status=200, reason="user created")
                content = {
                    "message": "User already exist"
                }
                return HttpResponse(json.dumps(content),status=202, reason="user Exist")
            else:
                return HttpResponse(status=403, reason="Error")
        else:
            return HttpResponse(status=403, reason="GET Method not allowed")


class AvailableProjects(APIView):
    # permission_classes = (IsAuthenticated,)
    def get(self, request):
            cursor_p = connection.cursor()
            cursor_p.execute("SELECT * FROM vicente_lopez_project")
            if cursor_p.rowcount:
                data = cursor_p.fetchall()
                projects=[]
                for p in data:
                    project ={
                        "id":p[0],
                        "name":p[1],
                        "image":p[2]
                    }
                    projects.append(project)

                return HttpResponse(json.dumps(projects), status=200, content_type='application/json')
            else:
                return HttpResponse(reason="There are no projects", status=403)


class Content(APIView):
    # permission_classes = (IsAuthenticated,)

    def post(self, request):
        if bool(request.body):
            resource_data = json.loads(request.body)
            for r in resource_data:
                if "category" not in r:
                    return HttpResponse(status=400)
                consumption = Consumption(**r)
                insert = ContentConsumption.create(category=consumption.category,
                                                   content_type=consumption.content_type,
                                                   content_name=consumption.content_name,
                                                   school_code=get_school_code(consumption.latitude, consumption.longitude),
                                                   date=consumption.date,
                                                   time=consumption.time,
                                                   latitude=consumption.latitude,
                                                   longitude=consumption.longitude,
                                                   serial_tablet =consumption.serial_tablet)
                insert.save()
            return HttpResponse(status=201)
        else:
            return HttpResponse(status=403)

    def get(self, request):
            try:
                if ((request.GET.get('dateFrom') is not None) & (request.GET.get('dateFrom') != "")) & ((request.GET.get('dateTo') is not None) & (request.GET.get('dateTo') != "")):
                    datetime.datetime.strptime(request.GET.get('dateFrom'), '%Y-%m-%d')
                    datetime.datetime.strptime(request.GET.get('dateTo'), '%Y-%m-%d')
                return HttpResponse(content_analytics.calculate_content_consumption(request.GET.get('category'),
                                                                            request.GET.get('dateFrom'),
                                                                            request.GET.get('dateTo'), request.GET.get('school_filter')),
                                    content_type='application/json')
            except:
                return HttpResponse(status=500, reason="Bad date format")

class TabletHistory(APIView):
    # permission_classes = (IsAuthenticated,)

    def get (self, request):
            return HttpResponse(launcher_analytics.tablet_today())


class ContentConsumed(APIView):
    # permission_classes = (IsAuthenticated,)

    def get (self, request):
        return HttpResponse(launcher_analytics.content_consumed_today())


class LauncherUse (APIView):
    # permission_classes = (IsAuthenticated,)

    def post (self, request):
        if bool(request.body):
            launcher_data = json.loads(request.body)
            for l in launcher_data:
                if "seconds_elapsed" not in l:
                    return HttpResponse(status=400)
                use = Use(**l)
                insert = Launcher.create(seconds_elapsed=use.seconds_elapsed,
                                         date=use.date,
                                                   time=use.time,
                                                   latitude=use.latitude,
                                                   longitude=use.longitude,
                                                   serial_tablet=use.serial_tablet)
                insert.save()
            return HttpResponse(status=201)
        else:
            return HttpResponse(status=403)


    def get (self, request):
        return HttpResponse(launcher_analytics.launcher_use())


class UseApp (APIView):
    def post(self,request):
        if bool(request.body):
            use = json.loads(request.body)
            for a in use:
                app = AppUse(**a)
                insert = AppActivity.create(app_name=app.app_name,
                                                date=app.date,
                                                end_time=app.end_time,
                                                init_time=app.init_time,
                                                latitude=app.latitude,
                                                longitude=app.longitude,
                                                serial_tablet=app.serial_tablet,
                                                time=app.time,
                                               category=app.category)
                insert.save()
            return HttpResponse(status=200, reason="Success")
        else:
            return HttpResponse(status=403)


class DownloadApp(APIView):
    def post(self, request):
        if bool(request.body):
            dowload = json.loads(request.body)
            for a in dowload:
                appDow = AppDownload(**a)
                insert = AppStatus.create(app_name=appDow.app_name,
                                            category =appDow.category,
                                            date=appDow.date,
                                            external_app =appDow.external_app,
                                            latitude=appDow.latitude,
                                            longitude=appDow.longitude,
                                            serial_tablet=appDow.serial_tablet,
                                            time=appDow.time,
                                            uninstalled=appDow.uninstalled)
                insert.save()
            return HttpResponse(status=200, reason="Success")
        else:
            return HttpResponse(status=403)



class ContentDownload(APIView):
    # permission_classes = (IsAuthenticated,)
    def post (self, request):
        if request.method == "POST":
            if bool(request.body):
                content_data = json.loads(request.body)
                for r in content_data:
                    download = DownloadContent(**r)
                    insert = ContentDownloads.create(category=download.category,
                                                     content_name=download.content_name,
                                                     date=download.date,
                                                     time=download.time,
                                                     latitude=download.latitude,
                                                     longitude=download.longitude,
                                                     school_code=download.school_code,
                                                     serial_tablet=download.serial_tablet)
                    insert.save()
                return HttpResponse(status=200, reason="Success")
        else:
            return HttpResponse(status=403, reason="GET Method not allowed")


class SearchesWords(APIView):
    def post(self, request):
            if bool(request.body):
                words_data = json.loads(request.body)
                for w in words_data:
                    word = Word(**w)
                    insert = WordSearches.create(word_searched=word .word_searched,
                                                 result=word.result,
                                                 date=word.date,
                                                 time=word.time,
                                                 latitude=word.latitude,
                                                 longitude=word.longitude,
                                                 serial_tablet=word.serial_tablet)
                    insert.save()
                return HttpResponse(status=200, reason="Success")
            else:
                return HttpResponse(status=403)
        
class DisabledEnabledUser(APIView):

    # permission_classes = (IsAuthenticated,)
    def post(self, request):
        user_data = json.loads(request.body)
        user = User(**user_data)
        mod = User.objects.get(id=user.id)

        mod.is_active = user.is_active
        mod.save()

        return HttpResponse(json.dumps({"menssage": "User disable successfully"}), status=200, reason="Success",
                            content_type='application/json')